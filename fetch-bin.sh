#!/bin/bash

mkdir -p "bin"

# download and minimize binaries
curl -L -o bin/cosign https://github.com/sigstore/cosign/releases/download/v1.11.1/cosign-linux-amd64
chmod +x bin/cosign

# minimize all binaries
upx bin/* || true
