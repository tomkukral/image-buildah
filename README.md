# image-buildah

Image for building another images - with podman, buildah and skopeo

## Signatures

Image can be verified by
```
cosign verify --key https://sigstore.6shore.net/tomkukral/buildah/release-cosign.pem <image-name>
# for example
cosign verify --key https://sigstore.6shore.net/tomkukral/buildah/release-cosign.pem tomkukral/buildah:0.32
```

Signature can be verified from
* https://gitlab.com/tomkukral/image-buildah/-/blob/master/release-cosign.pem
* https://sigstore.6shore.net/tomkukral/buildah/release-cosign.pem
