FROM fedora:38

RUN dnf install -y \
    buildah-1.30.0 \
    bzip2 \
    findutils \
    gettext \
    git \
    jq \
    make \
    parallel \
    podman-4.5.0 \
    qemu-user-static \
    skopeo-1.11.2 \
    unzip \
    which \
    xz \
  && curl https://raw.githubusercontent.com/multiarch/qemu-user-static/master/containers/latest/register.sh > /register \
  && curl https://raw.githubusercontent.com/qemu/qemu/e75941331e4cdc05878119e08635ace437aae721/scripts/qemu-binfmt-conf.sh > /qemu-binfmt-conf.sh \
  && chmod +x /register /qemu-binfmt-conf.sh \
  && rm -rf \
    /usr/lib64/python*/__pycache__/ \
    /var/cache/dnf/ \
    /var/log/dnf*.log


COPY bin /usr/local/bin
COPY libpod.conf /etc/containers/libpod.conf
COPY podman_setup /usr/local/bin/podman_setup
COPY podman-wrapper /usr/local/bin/podman

RUN /usr/local/bin/podman_setup
